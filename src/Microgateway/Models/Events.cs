using System;

namespace Microgateway.Models
{
    public class Event
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}