﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microgateway.Hubs;
using Microgateway.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace Microgateway.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        private readonly IHubContext<EventsHub, IEventBroadCaster> _context;
        public EventsController(IHubContext<EventsHub, IEventBroadCaster> context)
        {
            _context = context;
        }


        // POST api/values
        [HttpPost]
        public Task Post([FromBody] Event @event)
        {
            _context.Clients.All.addEvent(@event.Id, @event.Name);
            return Task.CompletedTask;
        }
    }
}
