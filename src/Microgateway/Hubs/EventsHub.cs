using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace Microgateway.Hubs
{
    public interface IEventBroadCaster
    {
        Task addEvent(Guid eventId, string name);
        Task receiveVote(Guid eventId, int feedback);
    }

    public class EventsHub: Hub<IEventBroadCaster>
    {
        public Task Join(Guid eventId)
        {
            Groups.AddToGroupAsync(Context.ConnectionId, eventId.ToString());
            return Task.CompletedTask;
        }

        public Task SendVote(Guid eventId, int feedback)
        {
            Clients.Group(eventId.ToString()).receiveVote(eventId, feedback);
            return Task.CompletedTask;
        }
    }
}